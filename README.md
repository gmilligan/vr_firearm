# What is this? 

Learn how to make a virtual reality firearm demonstration.

# Applications

- Unreal Engine 4.25.3
- Gravity Sketch

# Table of Contents

## 001. Create a Firearm Asset

![Image](/docs/001/img/001.PNG)

- ### [Collect Reference Images](/docs/001/001.md)


![Image](/docs/001/img/002.PNG)

- ### [Create the Firearm in Gravity Sketch VR](/docs/001/002.md)


![Image](/docs/001/img/003.PNG)

- ### [Export from Gravity Sketch into Blender](/docs/001/003.md)


![Image](/docs/001/img/004.PNG)

- ### [Create Firearm Materials in Blender](/docs/001/004.md)


![Image](/docs/001/img/005.PNG)

- ### [Export Firearm from Blender into Unreal Engine](/docs/001/005.md)
